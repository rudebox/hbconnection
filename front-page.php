<?php get_template_part('parts/header'); ?>

<main>

  <?php get_template_part('parts/page', 'header');?>

  <?php get_template_part('parts/link', 'boxes'); ?>

  <?php get_template_part('parts/content', 'layouts'); ?>

  <?php get_template_part('parts/cta'); ?>

  <?php get_template_part('parts/footer', 'gallery'); ?>

</main>

<?php get_template_part('parts/footer'); ?>
