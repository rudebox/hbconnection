<?php
	$img = get_field('news_img', 'options');
?>
	
<section class="contact padding--top">
	<div class="wrap hpad">
		<div class="row flex flex--wrap">

			<div class="col-sm-6 contact__item contact__item--form bg--grey-light">
				<?php gravity_form( 1, $display_title = true, $display_description = true, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); ?>
			</div>

			<div class="col-sm-6 contact__item contact__item--clients bg--grey">
				<?php if (have_rows('testimonials', 'options') ) : ?>
					<div class="contact__clients">
						<h2 class="contact__clients-title"><?php echo esc_html(the_field('clients_title', 'options')); ?></h2>
					<?php while (have_rows('testimonials', 'options') ) : the_row(); 
						$text = get_sub_field('text');
					?>
						<div class="contact__testimonial">
							<?php echo $text; ?>
						</div>
					<?php endwhile; ?>
					</div>		
				<?php endif; ?>		
			</div>

		</div>
	</div>
</section>

