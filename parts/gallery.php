<?php

	$gallery = get_field('offer_gallery');
	$i=0;
?>

<?php if ( $gallery ) : ?>

<div class="offer-gallery col-sm-6" itemscope itemtype="http://schema.org/ImageGallery">

	<div class="flex flex--wrap">

		<?php
			// Loop through gallery
			foreach ( $gallery as $image ) : 
			$i++;
		?>

			<figure class="offer-gallery__item offer-gallery__item--<?php echo esc_attr($i); ?>" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject" style="background-image: url(<?php echo esc_url($image['url']); ?>);">
				<a href="<?= $image['sizes']['large']; ?>" class="js-zoom product-gallery__link" data-fancybox="gallery-<?= $index; ?>" itemprop="contentUrl" title="<?= $image['title'] ?>">
				</a>
			</figure>

		<?php endforeach; ?>

	</div>

</div>

<?php endif; ?>
