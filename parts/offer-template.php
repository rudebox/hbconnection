<?php 
/*
 * Template Name: Ydelser
 */

get_template_part('parts/header'); the_post(); 

$id = (is_home()) ? get_option('page_for_posts') : $post->ID;
$title = get_proper_title($id);

$text = get_field('offer_text');
$related_links = get_field('offer_links');
$cta_text = get_field('offer_cta_text');
$gallery = get_field('offer_gallery');
?>


<main>
	<?php get_template_part('parts/page', 'header'); ?>

	<?php get_template_part('parts/link', 'boxes'); ?>

	<?php get_template_part('parts/breadcrumbs');?>

	<section class="offer padding--top">
		<div class="wrap hpad">
			<div class="row">

				<div class="col-sm-6 offer__text">
					<h1 class="page__title page__title--black"><?php echo the_title(); ?></h1>
					<?php echo $text; ?>
					<?php if ($cta_text) : ?>
					<div class="offer__cta">
						<?php echo $cta_text; ?>
					</div>
					<?php endif; ?>
				</div>
				
				<?php if ($related_links) : ?>
				<div class="col-sm-6">
					<div class="row offer__row">
						<?php foreach($related_links as $related_link) : ?>
							<a class="offer__related col-md-6" href="<?php echo the_permalink($related_link); ?>" style="background-image: url(<?php echo wp_get_attachment_image_url( get_post_thumbnail_id($related_link), 'url' );  ?>);">
								<div class="offer__title bg--yellow"><?php echo get_the_title( $related_link ); ?></div>
							</a>
						<?php endforeach; ?>
					</div>	
				</div>
				<?php endif; ?>

				<?php if ($gallery) : ?>
					<?php get_template_part('parts/gallery'); ?>
				<?php endif; ?>

			</div>
		</div>
	</section>		

	<?php get_template_part('parts/cta'); ?>

  	<?php get_template_part('parts/footer', 'gallery'); ?>

</main>

<?php get_template_part('parts/footer'); ?>