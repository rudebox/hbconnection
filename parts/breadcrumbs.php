<section class="breadcrumbs">
	<div class="wrap hpad">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
  				yoast_breadcrumb( '<div class="breadcrumbs__list">','</div>' );
			}
		?>
	</div>
</section>