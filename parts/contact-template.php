<?php 
/*
 * Template Name: Kontakt
 */

get_template_part('parts/header'); the_post(); 


$text = get_field('contact_text');
?>


<main>
	<?php get_template_part('parts/page', 'header'); ?>

	<?php get_template_part('parts/link', 'boxes'); ?>

	<?php get_template_part('parts/breadcrumbs');?>

	<section class="offer padding--both">
		<div class="wrap hpad">
			<div class="row">

				<div class="col-sm-6 offer__text">
					<h1 class="page__title"><?php echo the_title(); ?></h1>
					<?php echo $text; ?>
				</div>

				<div class="col-sm-6">
				<?php gravity_form( 1, $display_title = false, $display_description = true, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); ?>
				</div>

			</div>
		</div>
	</section>		

  	<?php get_template_part('parts/footer', 'gallery'); ?>

</main>

<?php get_template_part('parts/footer'); ?>