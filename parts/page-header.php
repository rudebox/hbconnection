<?php 
	if ( is_tax() ) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$title = $term->name;
	} elseif ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_archive() ) {
		$title = post_type_archive_title( '', false );
	} elseif ( is_404() ) {
		$title = __('Siden kunne ikke findes', 'lionlab');
	} elseif ( is_search() ) {
		$title = __('Søgeresultat', 'lionlab');
	} else {
		$id = (is_home()) ? get_option('page_for_posts') : $post->ID;
		$title = get_proper_title($id);
	}

	$page_img = get_field('page_img') ? : $page_img = get_field('page_img', 'options');
	$page_link = get_field('page_link');
	$page_link_text = get_field('page_link_text');
?>

<section class="page__hero" style="background-image: url(<?php echo esc_url($page_img['url']); ?>);">
	<div class="wrap hpad">
		<?php if (is_front_page() || is_page_template('page-layouts.php') )  : ?>
			<h1 class="page__title"><?php echo $title; ?></h1>
		<?php else: ?>
			<h2 class="page__title h1"><?php echo $title; ?></h2>
		<?php endif; ?>
		<?php if ($page_link) : ?>
			<a class="btn btn--yellow" href="<?php echo esc_url($page_link) ?>"><?php echo esc_html($page_link_text); ?></a>
		<?php endif; ?>
	</div>
</section>