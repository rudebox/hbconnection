<?php 
/**
* Description: Lionlab link-boxes repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$title = get_sub_field('header');

if (have_rows('linkbox') ) :
?>

<section class="quicklinks <?php echo esc_attr($bg); ?>--bg padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<?php if ($title) : ?>
		<h2 class="quicklinks__header"><?php echo esc_html($title); ?></h2>
		<?php endif; ?>
		<div class="row flex flex--wrap">
			<?php while (have_rows('linkbox') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
				$link = get_sub_field('link');
			?>

			<a href="<?php echo esc_url($link); ?>" class="col-sm-4 quicklinks__item bg--grey">			
				<h3 class="quicklinks__title"><?php echo esc_html($title); ?></h3>
				<?php echo $text; ?>
			</a>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>