<?php
 /**
   * Description: Lionlab employees repeater field group
   *
   * @package Lionlab
   * @subpackage Lionlab
   * @since Version 1.0
   * @author Kaspar Rudbech
   */
 

 //section settings
$header = get_sub_field('header');
$text = get_sub_field('text');
$margin = get_sub_field('margin');

 if (have_rows('employee') ) :
?>

<section class="employees bg--<?php echo esc_attr($bg); ?> padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<?php if ($header) : ?>
			<h2 class="employees__header yellow center"><?php echo esc_html($header); ?></h2>
		<?php endif; ?>
		<?php if ($text) : ?>
			<div class="employees__header-text center">
				<?php echo $text; ?>
			</div>
		<?php endif; ?>
		<div class="row clearfix flex flex--wrap">
		<?php while (have_rows('employee') ) : the_row(); 
			$img = get_sub_field('image');
			$name = get_sub_field('name');
			$position = get_sub_field('text');
			$mail = get_sub_field('mail');
			$areas = get_sub_field('areas');
			$phone = get_sub_field('phone');
		?>

		<div class="col-sm-3 employees__item">
			<div class="employees__img" style="background-image: url(<?php echo esc_url($img['url']); ?>);"></div>
			<div class="employees__content bg--black">
				<p class="employees__name"><?php echo esc_html($name); ?></p>
				<p class="employees__position"><?php echo esc_html($position); ?></p>
				<p class="employees__areas"><?php echo esc_html($areas); ?></p>
				
				<?php if ($mail) : ?>
				<a class="employees__mail" href="mailto:<?php echo esc_html($mail); ?>"><?php echo esc_html($mail); ?></a>
				<?php endif; ?>

				<?php if ($phone) : ?>
				<a class="employees__phone" href="tel:<?php echo get_formatted_phone($phone); ?>"><?php echo esc_html($phone); ?></a>
				<?php endif; ?>
				
			</div>
		</div>
		<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>