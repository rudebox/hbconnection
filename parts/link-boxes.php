<?php 
/**
* Description: Lionlab link-boxes repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/


if (have_rows('linkbox', 'options') ) :
?>

<section class="link-boxes bg--blue padding--both">
	<div class="link-boxes__element link-boxes__element--left"></div>
	<div class="link-boxes__element link-boxes__element--right"></div>
	<div class="wrap hpad">
		<h2 class="link-boxes__header"><?php echo esc_html($title); ?></h2>
		<div class="row flex flex--wrap">
			<?php while (have_rows('linkbox', 'options') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
				$icon = get_sub_field('icon');
			?>

			<div class="col-sm-4 link-boxes__item">
				<?php if ($icon) : ?>
				<img class="link-boxes__icon" src="<?php echo esc_url($icon['url']); ?>" alt="<?php echo esc_attr($icon['alt']); ?>">
				<?php endif; ?>
				<h3 class="link-boxes__title"><?php echo esc_html($title); ?></h3>
				<?php echo $text; ?>
			</div>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>