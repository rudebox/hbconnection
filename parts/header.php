<!doctype html>

<html <?php language_attributes(); ?> class="no-js">

<head>
  <meta charset="utf-8">
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> itemscope itemtype="https://schema.org/WebPage">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TNQNHNH"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<?php 
  $phone = get_field('phone', 'options');
  $mail = get_field('mail', 'options');
?>

<div class="toolbar bg--grey">
  <div class="wrap hpad flex flex--end">

    <div class="toolbar__item">
      <a href="tel:<?php echo get_formatted_phone($phone); ?>">Tel. <?php echo esc_html($phone); ?></a>
    </div>
    <div class="toolbar__item">
      <a href="mailto:<?php echo esc_html($mail); ?>"><?php echo esc_html($mail); ?></a>
    </div>

  </div>
</div>

<header class="header" id="header" itemscope itemtype="http://schema.org/WPHeader">
  <div class="wrap hpad flex flex--center flex--justify">

    <?php 
      //logo SEO markup
      $site_name = get_bloginfo( 'name' );
      $logo_markup  = ( is_front_page() ) ? '<h1 class="visuallyhidden">' . $site_name . '</h1>' : '<p class="visuallyhidden">' . $site_name . '</p>';
    ?>

    <a class="header__logo" href="<?php bloginfo('url'); ?>">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="<?php bloginfo('name'); ?>">
      <?php echo $logo_markup; ?>
    </a>

    <div class="nav-toggle"> 
      <span class="nav-toggle__label"><?php _e('Menu', 'lionlab') ?></span> 
      <span class="nav-toggle__icon"></span>
    </div>

    <nav class="nav" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
      <div class="nav--mobile">
        <?php scratch_main_nav(); ?>
      </div>
    </nav>

  </div>
</header>
